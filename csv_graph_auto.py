# Matplotlib = 1.4.3, Numpy = 1.9.2
import matplotlib.pyplot as plt
from matplotlib.dates import date2num
import numpy as np
import csv
import datetime
from time import time


def create_graph(column_list, list_of_xy):
    #   Create numpy array from list_of_xy
    arr_xy = np.array(list_of_xy)

    #   Sets up each graph one at a time. Then saves a picture of graph to current directory.
    count = 0
    for i in range(0, len(arr_xy), 2):
        j = i + 1
        x_values = arr_xy[i]
        y_values = arr_xy[j]
        y_label = column_list[count]

        plt.plot_date(x=x_values, y=y_values, linestyle='-', marker="None", markevery=200)
        plt.gcf().autofmt_xdate()
        plt.title(y_label)
        plt.xlabel("timestamp")
        plt.ylabel(y_label)
        count += 1
        plt.legend()
        plt.minorticks_off()
        plt.grid()

    #   tight_layout() does not work with MacOSX
    #   Disable tightlayout() if using large data sets
    #   plt.tight_layout()

        cur_time = datetime.datetime.now()
        plt.savefig("Figure_" + y_label + "_" + cur_time.strftime("%Y_%m_%d_%H_%M_%S") + ".png")
#        plt.show()
        plt.close()
        plt.figure()


def create_list(csv_file, indexes, plots, offset):
    #       Create a list of lists, where each list is column of data.
    #       Converts y values into floats, and x values into datetime format
    #       Each index loop resets the file pointer to after the header
    temp_list_x = []
    temp_list_y = []
    list_of_xy = []

    for i in indexes:
        for row in plots:
            try:
                item_y = row[i]
                y = float(item_y)
                dt = date2num(datetime.datetime.strptime(row[0], '%Y-%m-%d %H:%M:%S'))
                temp_list_x.append(dt)
                temp_list_y.append(y)
            except ValueError:
                pass

        list_of_xy.append(temp_list_x)
        list_of_xy.append(temp_list_y)

        csv_file.seek(offset, 0)
        temp_list_x = []
        temp_list_y = []

    return list_of_xy


def csv_graph(file_name, column_list):
    #   Open csv file and get index number of column names
    indexes = []
    with open(file_name) as csv_file:
        plots = csv.reader(csv_file, delimiter=',')
        header = next(plots)

        for column in column_list:
            count = 0
            for name in header:
                if name == column:
                    indexes.append(count)
                    print indexes
                count += 1

        offset = len(header)
        list_of_xy = create_list(csv_file, indexes, plots, offset)

    create_graph(column_list, list_of_xy)


def main():
    file_name = raw_input("Enter csv file name: ")
    start_time = time()
    column_names = "load_min15,memswap_percent,percpu_0_total,percpu_1_total,percpu_2_total,percpu_3_total,percpu_4_total,percpu_5_total,percpu_6_total,percpu_7_total,mem_percent"
    column_list = column_names.translate(None, "()").split(",")

    csv_graph(file_name, column_list)

    print("--- %s seconds ---" % (time() - start_time))


if __name__ == "__main__":
    main()
