# Program is used to create graphs from csv files using matplotlib
import matplotlib.pyplot as plt
import numpy as np
import sys
import csv
import datetime


# Sets up the graph, displays it and saves a .png file of the graph
# in the format "Figure_x_y_YEAR_MONTH_DAY_HOUR_MIN.png"
def print_graph(column_list, list_of_xy):
    clean_name_col0 = column_list[0].strip()
    clean_name_col1 = column_list[1].strip()

    plt.plot(*list_of_xy, label=("(" + clean_name_col0 + ", " + clean_name_col1) + ")", marker='o')
    if clean_name_col0 == "timestamp":
        plt.gcf().autofmt_xdate()
    plt.xlabel(clean_name_col0)
    plt.ylabel(clean_name_col1)
    plt.title("Graph")
    plt.legend()
    plt.minorticks_off()
    plt.grid()

    #plt.tight_layout() may slow down processing of large datasets. Recommend turning this off if program is slow
    plt.tight_layout()

    if len(list_of_xy[0]) <= 20:
        plt.xticks(np.arange(0, len(list_of_xy[0]), step=1))
    else:
        plt.xticks(np.arange(0, len(list_of_xy[0]), step=len(list_of_xy[0]) / 10 + 1))

    cur_time = datetime.datetime.now()
    plt.savefig("Figure_" + clean_name_col0 + "_" + clean_name_col1 + "_" + cur_time.strftime("%Y_%m_%d_%H_%M") + ".png")
    plt.show()


def create_graph(file_name, column_list):
    indexes = []
    print column_list

    # Open csv file
    with open(file_name) as csv_file:
        plots = csv.reader(csv_file, delimiter=',')  # Create reader object
        header = next(plots)  # Get header of file

        # Now find which index in the header has a column name that matches the user input columns
        for column in column_list:
            count = 0
            for name in header:
                if name == column:
                    indexes.append(count)
                count += 1
        print indexes

        list_of_xy = []
        temp_list = []
        offset = len(header)  # Start the offset after the header
        y_val = False

        # For loop looks indexes from user input and appends the data from the column into temp_list.
        # If temp_list contains the y value of a line, then the elements in that list will be casted as float
        # so that pyplot sorts the y-axis correctly.
        # The temp_list is then appended to list_of_xy for both x and y values.
        for i in indexes:
            for row in plots:
                temp_list.append(row[i])
                offset += len(row)
            if y_val is True:
                float_list = [float(i) for i in temp_list]
                list_of_xy.append(float_list)
            else:
                list_of_xy.append(temp_list)
                y_val = True

            # Moves the file pointer back to start of file, but after the header.
            csv_file.seek(0)
            next(plots)
            temp_list = []

    print_graph(column_list, list_of_xy)


#User enters column names in either for mat "(X,Y)" or "X,Y".
#Multiple lines can be draw for a graph if the the X coordinate is the same for all lines
def csv_graph():
    file_name = raw_input("Enter csv filename: ")
    column_names = raw_input("Enter enter column names as (x,y) to graph, comma separated for multiple lines"
                             "\n(Ex: '(time,apples),(year,median)'):\n")
    column_list = column_names.translate(None, "()").split(",")

    if len(column_list) % 2 != 0:
        sys.exit("Uneven number of coordinates")

    create_graph(file_name, column_list)


def main():
    csv_graph()


if __name__ == "__main__":
    main()
